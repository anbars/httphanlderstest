﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace HttpHanldersTest.HttpHandler
{
    public class MyHttpHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            if (context.Request.QueryString.Count > 3)
            {
                context.Response.Write("<h1>Error!</h1>");
                context.Response.Write("<p>Requests with more then 3 query parameters are not allowed</p>");
            }
        }

        public bool IsReusable => true;
    }
}
