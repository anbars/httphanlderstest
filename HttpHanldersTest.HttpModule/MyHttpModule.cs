﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace HttpHanldersTest.HttpModule
{
    public class MyHttpModule : IHttpModule
    {
        private Dictionary<HttpRequest, DateTime> _trackedRequests = new Dictionary<HttpRequest, DateTime>();
        public void Init(HttpApplication context)
        {
            context.BeginRequest += Context_BeginRequest;
            context.EndRequest += ContextOnEndRequest;
        }

        private void ContextOnEndRequest(object sender, EventArgs e)
        {
            var application = (HttpApplication) sender;
            HttpRequest request = application.Context.Request;
            if (_trackedRequests.ContainsKey(request))
            {
                var response = application.Context.Response;
                //response.ClearContent();
                //response.Write("<h1>Request info</h1>");
                //response.Write($"<p>Relative URI: {request.Path}</p>");
                //response.Write($"<p>Request time: {(DateTime.UtcNow - _trackedRequests[request]).TotalMilliseconds} mls.</p>");
                _trackedRequests.Remove(request);
            }
        }

        private void Context_BeginRequest(object sender, EventArgs e)
        {
            var application = (HttpApplication) sender;
            _trackedRequests.Add(application.Context.Request, DateTime.UtcNow);
        }

        public void Dispose()
        {
            //nothing to do here
        }
    }
}
